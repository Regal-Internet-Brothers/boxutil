boxutil
=======

A simple module for the [Monkey programming language](https://github.com/blitz-research/monkey) containing useful routines for dealing with the standard ['monkey.boxes'](https://github.com/blitz-research/monkey/blob/develop/modules/monkey/boxes.monkey) module.
